package map;


import java.util.*;  
public class map {  
public static void main(String[] args) {  
	
    Map map=new HashMap();  
    
    //Adding elements to map 
    
    map.put(1,"Pangare");  
    map.put(11,"Pangare");
    map.put(5,"Crioulo");  
    map.put(3,"Manga Larga");  
    map.put(9,"Manga Larga");
    
    Set set=map.entrySet(); 
    Iterator itr=set.iterator();  
    while(itr.hasNext()){  
       
        Map.Entry entry=(Map.Entry)itr.next();  
        System.out.println(entry.getKey()+" "+entry.getValue());  
    }  
}  
}