package Cavalo;

public class Cavalo {

//atributos	

	
	private char porte;
	private String ra�a;
	private char genero;
	private float peso;
	private String cor;
	private int idade;
	private boolean vacina;

	
	public char getPorte() {
		return porte;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float p) {
		peso = p;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public boolean isVacina() {
		return vacina;
	}

	public void setVacina(boolean vacina) {
		this.vacina = vacina;
	}
	
//m�todo construtor	

	public Cavalo(String ra�a, String cor1, char gen){
		this.ra�a=ra�a;
		cor=cor1;
		this.genero=gen;
		
	}

//m�todos de classes	
	public void relinchar(){
		System.out.println("Riinch, Riinch, Riinch");
	}
	
	public void abanarRabo(){
		System.out.println("Abanar,abanar, abanar...");
	}
	
	public boolean Rodar(boolean obd){
		if (obd) {
			System.out.println("Vamos Rodar!");
		}
		else
		{
			System.out.println("Parado!");
		}
		
		return obd;
	}
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		
		Cavalo o1=new Cavalo("manga larga","cinza",'F');
		o1.setIdade(4);
		Cavalo o2=new Cavalo("pangar�","bege",'F');
		o2.relinchar();
		Cavalo o3=new Cavalo("crioulo","branco",'M');
		o3.Rodar(false);
		Cavalo o4=new Cavalo("manga larga","preto",'F');
		o4.Rodar(true);
	}
}