package list;

import java.util.*;  

public class list {  
	
public static void main(String args[]){  
 //Creating a List  
 List<String> list=new ArrayList<String>();  
 //Adding elements in the List  
 list.add("Manga Larga");  
 list.add("Pangare");  
 list.add("Crioulo");  
 list.add("Marchador");  
 //Iterating the List element using for-each loop  
 for(String raca:list)  
  System.out.println(raca);  
  
}  
}